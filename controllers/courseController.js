const Course = require("../models/Course");


/*module.exports.addCourse = (reqBody) => {

	let newCourse = new Course ({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	})
	
	return newCourse.save().then((course, error) => {
		if(error){
			return false;

		} else {
			return true;
		};
	});
};*/

module.exports.addCourse = (data) => {

	if(data.isAdmin) {

	let newCourse = new Course ({
		name : data.course.name,
		description : data.course.description,
		price : data.course.price
	})
	
	return newCourse.save().then((course, error) => {
		if(error){
			return false;

		} else {
			return {
				message: "New course successfully created!"
			}
		};
	});
};

let message = Promise.resolve({
	message: "User must be an Admin to access this!"
})

return message.then((value) => {
	return value;
})

}

module.exports.getAllCourse = () => {
	return Course.find({}).then(result => {
		return result
	})
}

module.exports.getAllActive = () => {
	return Course.find({isActive : true}).then(result => {
		return result
	})
}

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result
	})
}

module.exports.updateCourse = (reqParams, reqBody) => {

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Course.findByIdAndUpdate(reqParams.id, updatedCourse).then((course, error) => {

		if(error){
			return false
		} else {
			let message = `Successfully updated course Id - "${reqParams.id}"`

			return message;
		}
	})
}

 //Activity 40
/*module.exports.archiveCourse = (reqParams, reqBody) => {

	let archivedCourse = {
		isActive: reqBody.isActive
		
	}

	return Course.findByIdAndUpdate(reqParams.id, archivedCourse).then((course, error) => {
		 
		 if(error) {
		 	return false
		 } else {
		 	return true
		 }
	})
}*/

module.exports.archiveCourse = (reqParams) => {

	let updateActiveField = {
		isActive: false
		
	}

	return Course.findByIdAndUpdate(reqParams.id, updateActiveField).then((course, error) => {
		 
		 if(error) {
		 	return false;
		 } else {
		 	return true;
		 };
	});
};