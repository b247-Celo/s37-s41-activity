	const express = require("express");
	const mongoose = require("mongoose");
	const cors = require("cors");
	const userRoutes = require("./routes/userRoutes");
	const courseRoutes = require("./routes/courseRoutes");

	const app = express();
	const port = 4000;

	app.use(cors());

	// Allows us to read json data
	app.use(express.json());

	// Allows us to read data from forms
	app.use(express.urlencoded({extended:true}));



	mongoose.connect("mongodb+srv://admin1:admin123@zuitt-bootcamp1.l5iqt9p.mongodb.net/S37-S41Activity?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
	);

	mongoose.connection.once("open",() => console.log('Now connected to MongoDB Atlas'));

	app.use("/users", userRoutes)
	app.use("/courses", courseRoutes)

			

	app.listen(port, () => console.log(`Now listening to port ${port}`));
		