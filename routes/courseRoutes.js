const express = require("express");
const router = express.Router();

const auth = require("../auth")

//Route for creating a course
const courseController = require("../controllers/courseController");

/*router.post("/", auth.verifyIfAdmin,(req,res) => {	

	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
});*/

//Route for creating a course
router.post("/", auth.verify,(req,res) => {	

	const data = {
		course : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(resultFromController => res.send(resultFromController))
});

// Route for retrieving all the courses
router.get("/all", (req,res) => {

	courseController.getAllCourse().then(resultFromController => res.send(resultFromController))
})

// Route for retrieving all active courses
router.get("/active", (req,res) => {

	courseController.getAllActive().then(resultFromController => res.send(resultFromController))

})

// Route for retrieving a specific course
router.get("/:courseId/details", (req,res) => {

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))

})

//Route for updating a course with admin authentication
router.put("/:id", auth.verify, (req,res) => {

	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))

})

//S40 Activity - Route for archiving a course
/*router.put("/:id/archive", auth.verify,(req,res) => {

	courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))

})*/

router.put("/:id/archive", auth.verify,(req,res) => {

	courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController))

})



module.exports = router;