	const express = require("express");
	const router = express.Router();

	const auth = require("../auth")

	const userController = require("../controllers/userController");


// Check the email route
	router.post("/checkEmail", (req,res) => {

			userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))

	});

// Register a user route
	router.post("/register", (req,res) => {

			userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
	});	

// Route for user login with authentication
	router.post("/login",(req,res) => {

			userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
	});

//Activity S38 Route for retrieving user details
	router.post("/details", auth.verify, (req,res) => {

			const userData = auth.decode(req.headers.authorization);
			console.log(userData);

			userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController))

	});

	//Route for enrolling a user (S41 Activity)
	router.post("/enroll", auth.verify, (req,res) => {

		/*let data = {
			userId : req.body.userId,
			courseId : req.body.courseId	
		}*/

		userData = auth.decode(req.headers.authorization)

		let data = {
			userId : userData.id,
			courseId : req.body.courseId	
		}

	
			userController.enroll(data).then(resultFromController => res.send(resultFromController));

	});


	module.exports = router;
	